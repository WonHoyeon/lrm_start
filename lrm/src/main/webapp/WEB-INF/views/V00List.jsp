<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>


<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">

				<div class="callout callout-info" style="width: 30%; align: center">

					<form class="form-horizontal" method="post" action="V00List">
						<div class="input-group input-group-sm mb-0">
							<input class="form-control form-control-sm"
								placeholder="학번 또는 ID" name="hakbun" maxlength="8">
							<div class="input-group-append">
								<button type="submit" class="btn btn-success">조회</button>
							</div>
						</div>
					</form>
				</div>

				<div class="card">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">이용수칙 위반내역 :</h3>
						</div>
					</div>
					<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>No.</th>
									<th>위반일자</th>
									<th>위반구분</th>
									<th>위반장소</th>
									<th>위반내역</th>
									<th>입력일자</th>
									<th>입력자</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
	                		<c:when test="${vInfo.size() > 0}">
	                			<c:forEach items="${vInfo}" var = "row" varStatus = "status">
								<tr>
									<td>1</td>
									<td>${row.PDATE}</td>
									<td>${row.CODE}</td>
									<td>${row.PLACE}</td>
									<td>${row.BIGO}</td>
									<td>${row.IDATE}</td>
									<td>${row.IADMIN}</td>
									<td class="text-right py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a href="#" class="btn btn-danger"><i
												class="fas fa-trash"></i></a>
										</div>
									</td>
								</tr>
								</c:forEach>
								</c:when>
								<c:otherwise>
               					<tr>
               					</tr>
               					</c:otherwise>
               					</c:choose>
							</tbody>
						</table>
						 <c:if test="${vInfo.size() == 0 }">
                  <div class="card card-primary">
                    <div class="card-header">
                      <h4 class="card-title" style="font-size:1.0rem; text-align:center;">데이터가 없습니다.</h4>
                    </div>
                  </div>
	              </c:if>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->

				<div class="card">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">추가입력</h3>
						</div>
					</div>
					<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>위반일자</th>
									<th>위반구분</th>
									<th>위반장소</th>
									<th>위반내역</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"> <i
													class="far fa-calendar-alt"></i>
												</span>
											</div>
											<input type="text" class="form-control float-right"
												id="reservation">
										</div>
									</td>
									<td><select class="form-control select2 select2-danger"
										data-dropdown-css-class="select2-danger" style="width: 100%;">
											<option selected="selected">세미나실 예약부도</option>
											<option>음식물 반입</option>
											<option>출입증 및 계정 대여</option>
											<option>도서관 부정 이용</option>
											<option>인쇄물 배포 및 물품 판매</option>
											<option>훼손 및 무단반출</option>
											<option>흡연</option>
									</select></td>
									<td><div class="form-group">
											<input	type="text" class="form-control" id="place">
										</div></td>
									<td><div class="form-group">
											<input type="text" class="form-control" id="details">
										</div></td>

									<td class="text-right py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a href="#" class="btn btn-info"><i class="fas fa-eye"></i></a>
										</div>
									</td>
								</tr>

							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>


				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">위반자 입력(개인별)</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool"
								data-card-widget="collapse" data-toggle="tooltip"
								title="Collapse">
								<i class="fas fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body p-0">
						<p>
							<br /> &nbsp;&nbsp; •개인별 위반내역 입력이 가능합니다. <br />&nbsp;&nbsp; •학번
							또는 이용자ID를 입력하고 검색을 누르면 위반내역을 입력할 수 있습니다. <br />
						</p>



					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->


			</div>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>



<%@ include file="/WEB-INF/views/outline/footer.jsp"%>