<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="java.io.PrintWriter" %>
<%@ include file="/WEB-INF/views/outline/header.jsp" %>




	<section class="hero-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="hs-text">
						<h4>도서관 이용수칙 위반 관리 시스템</h4>
						<p>- 이 프로그램은 관리자로 권한인증된 기기만 사용 가능합니다.</p>
						</div>
				</div>
			<%
				if(userID == null){
			%>		
		<div class="col-lg-6">
					<form class="hero-form" method="post" action="/loginAction">
						<input type="text" class="form-control" placeholder="ID" name="userID" maxlength="8">   
						<input type="password" class="form-control" placeholder="PASSWORD" name="password">
						<input type="submit" class="site-btn" value="로그인">
					</form>
				</div>
				
			<%
				} else {
			%>
			<script>
			location.href = 'login.jsp';
			</script>
			
			<%
				}
			%>
				
			</div>
		</div>
		<div class="hero-slider owl-carousel">
			<div class="hs-item set-bg" data-setbg="resources/img/hero-slider/1.jpg"></div>
			<div class="hs-item set-bg" data-setbg="resources/img/hero-slider/2.jpg"></div>
			<div class="hs-item set-bg" data-setbg="resources/img/hero-slider/3.jpg"></div>
		</div>
	</section>



	<footer class="footer-section">
		<div class="container">
		   <div class="row">
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget">
						<h2>What we do</h2>
						<ul>
							<li><a href="#">Loans</a></li>
							<li><a href="#">Car loans</a></li>
							<li><a href="#">Debt consolidation loans</a></li>
							<li><a href="#">Home improvement loans</a></li>
							<li><a href="#"> Wedding loans</a></li>
							<li><a href="#">Innovative Finance ISA</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget">
						<h2>About us</h2>
						<ul>
							<li><a href="#">About us</a></li>
							<li><a href="#">Our story</a></li>
							<li><a href="#">Meet the board</a></li>
							<li><a href="#">Meet the leadership team</a></li>
							<li><a href="#">Awards</a></li>
							<li><a href="#">Careers</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget">
						<h2>Legal</h2>
						<ul>
							<li><a href="#">Privacy policy</a></li>
							<li><a href="#">Loans2go principles</a></li>
							<li><a href="#">Website terms</a></li>
							<li><a href="#">Cookie policy</a></li>
							<li><a href="#">Conflicts policy</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget">
						<h2>Site Info</h2>
						<ul>
							<li><a href="#">Support</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Careers</a></li>
							<li><a href="#">Contact us</a></li>
						</ul>
					</div>
				</div>
		   </div>
			
		</div>
	</footer>
	

<%@ include file="/WEB-INF/views/outline/footer.jsp" %>
