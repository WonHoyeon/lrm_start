<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.io.PrintWriter"%>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">

				<div class="callout callout-info">
					<form class="form-horizontal" method="post" action="v1_check"
						style="display: flex">
						<div class="info-box-content"
							style="text-align: center; width: 25%; flex: 3">처리 기준 일자</div>
						<div class="input-group"
							style="text-align: center; width: 25%; flex: 4">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i
									class="far fa-calendar-alt"></i>
								</span> <input type="text" class="form-control float-right"
									id="reservation2">
							</div>
						</div>
						<div class="input-group-append"
							style="text-align: center; width: 25%; flex: 5">
							<button type="submit" class="btn btn-success">조회</button>
						</div>
					</form>
				</div>


				<div class="card">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">위반자 처벌 결정</h3>
						</div>
					</div>
					<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>No.</th>
									<th>소속</th>
									<th>학번</th>
									<th>이름</th>
									<th>위반구분</th>
									<th>횟수</th>
									<th>징계내역</th>
									<th>징계기간</th>
									<th>확정여부</th>
									<th>위반일</th>
									<th>기준일</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>국제행정학과</td>
									<td>1234567899</td>
									<td>홍길동</td>
									<td>세미나실 예약부도</td>
									<td>3</td>
									<td><div style="min-width: 160px;">
											<input style="width: 60px; display: inline-block"
												type="number" min="0" id="inputEstimatedBudget"
												class="form-control" value=""> <select
												class="form-control select2 select2-danger"
												data-dropdown-css-class="select2-danger"
												style="width: 115px; display: inline-block">
												<option selected="selected">경고</option>
												<option>일 정지</option>
												<option>달 정지</option>
												<option>학기 정지</option>
												<option>변상</option>
											</select>
										</div></td>
									<td><div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"> <i
													class="far fa-calendar-alt"></i>
												</span> 
												</div><input type="text" class="form-control float-right"
													id="reservation2">
											</div>
									</td>
									<td><select class="form-control select2 select2-danger"
										data-dropdown-css-class="select2-danger" style="width: 100px;">
											<option selected="selected">미확정</option>
											<option>확정</option>
									</select></td>
									<td>2020-05-14</td>
									<td>2020-05-15</td>
									<td class="text-right py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a class="btn btn-info btn-sm" href="#"> <i
												class="fas fa-pencil-alt"></i></a> <a href="#"
												class="btn btn-danger"><i class="fas fa-trash"></i></a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->



				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">위반자 처벌 결정</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool"
								data-card-widget="collapse" data-toggle="tooltip"
								title="Collapse">
								<i class="fas fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body p-0">
						<p>
							<br /> &nbsp;&nbsp; •위반자처벌기초자료 생성처리로 생성된 위반자처벌자료를 최종 확정하는 단계입니다.
							<br /> &nbsp;&nbsp; •확정이 완료된 자료만 이용자가 조회 가능합니다. <br />
						</p>



					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->


			</div>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>


<%@ include file="/WEB-INF/views/outline/footer.jsp"%>