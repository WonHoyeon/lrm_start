<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.io.PrintWriter"%>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">

				<div class="callout callout-info">
					<form class="form-horizontal" method="post" action="v1_check"
						style="display: flex">
						<div class="info-box-content"
							style="text-align: center; width:30px; flex: 1">위반 구분</div>
						<select class="form-control select2 select2-danger"
							data-dropdown-css-class="select2-danger"
							style="width: 250px; flex: 2">
							<option selected="selected">세미나실 예약부도</option>
							<option>음식물 반입</option>
							<option>출입증 및 계정 대여</option>
							<option>도서관 부정 이용</option>
							<option>인쇄물 배포 및 물품 판매</option>
							<option>훼손 및 무단반출</option>
							<option>흡연</option>
						</select>
						<div class="info-box-content"
							style="text-align: center; flex: 3">출력 기간</div>
						<div class="input-group"
							style="text-align: center; width: 150px; flex: 4">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i
									class="far fa-calendar-alt"></i>
								</span> <input type="text" class="form-control float-right"
									id="reservation2">
							</div>
						</div>
						<div class="info-box-content"
							style="text-align: center; width: 20px; flex: 5">확정 유무</div>
						<select class="form-control select2 select2-danger"
							data-dropdown-css-class="select2-danger"
							style="width: 20px; display: inline-block; flex: 6">
							<option selected="selected">전체</option>
							<option>미확정</option>
							<option>확정</option>
						</select>
						<div class="input-group-append"
							style="text-align: center;">
							<button type="submit" class="btn btn-success">출력</button>
						</div>
						<div class="input-group-append"
							style="text-align: center;">
							<button type="submit" class="btn btn-success">파일</button>
						</div>
					</form>
				</div>




				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">처리내역 출력</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool"
								data-card-widget="collapse" data-toggle="tooltip"
								title="Collapse">
								<i class="fas fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body p-0">
						<p>
							&nbsp;&nbsp; •출력할 기간을 설정하고 출력버튼을 누르십시오. <br /> &nbsp;&nbsp; •기간별
							위반자 처리내역을 출력(또는 파일저장) 할 수 있습니다. <br /> &nbsp;&nbsp; •출력기간은
							처리기준일자입니다. <br />
						</p>



					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->


			</div>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>


















<%@ include file="/WEB-INF/views/outline/footer.jsp"%>