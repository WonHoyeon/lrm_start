<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>


<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">

				<div class="callout callout-info" style="width: 30%; align: center">

					<form class="form-horizontal" method="post" action="V00List">
						<div class="input-group input-group-sm mb-0">
							<input class="form-control form-control-sm"
								placeholder="학번 또는 ID" name="hakbun" maxlength="8">
							<div class="input-group-append">
								<button type="submit" class="btn btn-success">조회</button>
							</div>
						</div>
					</form>
				</div>




				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">위반자 입력(개인별)</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool"
								data-card-widget="collapse" data-toggle="tooltip"
								title="Collapse">
								<i class="fas fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body p-0">
						<p>
							<br /> &nbsp;&nbsp; •개인별 위반내역 입력이 가능합니다. <br />&nbsp;&nbsp; •학번
							또는 이용자ID를 입력하고 검색을 누르면 위반내역을 입력할 수 있습니다. <br />
						</p>



					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->


			</div>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>



<%@ include file="/WEB-INF/views/outline/footer.jsp"%>