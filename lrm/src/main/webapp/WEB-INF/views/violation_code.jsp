<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.io.PrintWriter"%>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>


<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">

				<div class="callout callout-info"style="width: 80%; align: center" >
					<form class="form-horizontal" method="post" action="v1_check" style="display: flex">
							<div class="info-box-content" style="text-align:center; width:25%; flex:1">위반구분</div>
						<select class="form-control select2 select2-danger"
							data-dropdown-css-class="select2-danger" style="flex:2">
							<option selected="selected">세미나실 예약부도</option>
							<option>음식물 반입</option>
							<option>출입증 및 계정 대여</option>
							<option>도서관 부정 이용</option>
							<option>인쇄물 배포 및 물품 판매</option>
							<option>훼손 및 무단반출</option>
							<option>흡연</option>
						</select>
							<div class="info-box-content" style="text-align:center; width:30px; flex:3">조회
								기간</div>
						<div class="input-group" style="text-align:center; width:250px; flex:4">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i
									class="far fa-calendar-alt"></i>
								</span>
							<input type="text" class="form-control float-right"
								id="reservation2">
								</div>
						</div>
						<div class="input-group-append" style="text-align:center;">
							<button type="submit" class="btn btn-success">조회</button>
						</div>
						</form>
				</div>
				
			
				<div class="card">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">{    }</h3>
						</div>
					</div>
					<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>No.</th>
									<th>위반일자</th>
									<th>소속</th>
									<th>학번</th>
									<th>이름</th>
									<th>위반장소</th>
									<th>위반내역</th>
									<th>입력일자</th>
									<th>입력자</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>2020-05-05</td>
									<td>국제행정학과</td>
									<td>2011445678</td>
									<td>김길동</td>
									<td>열람실</td>
									<td>5.4, 4.22</td>
									<td>2020-05-26</td>
									<td>z8013421</td>
									<td class="text-right py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a href="#" class="btn btn-danger"><i
												class="fas fa-trash"></i></a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->

				<div class="card">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">추가입력</h3>
						</div>
					</div>
					<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>위반일자</th>
									<th>학번</th>
									<th>위반장소</th>
									<th>위반내역</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"> <i
													class="far fa-calendar-alt"></i>
												</span>
											</div>
											<input type="text" class="form-control float-right"
												id="reservation">
										</div>
									</td>
									<td><div class="form-group">
											<input type="text" class="form-control"
												id="hakbun">
										</div></td>
									<td><div class="form-group">
											<input type="text" class="form-control"
												id="place">
										</div></td>
									<td><div class="form-group">
											<input type="text" class="form-control"
												id="details">
										</div></td>

									<td class="text-right py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a href="#" class="btn btn-info"><i class="fas fa-eye"></i></a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>


				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">위반자 입력(구분별)</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool"
								data-card-widget="collapse" data-toggle="tooltip"
								title="Collapse">
								<i class="fas fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body p-0">
						<p>
							<br /> &nbsp;&nbsp; •위반구분별로 위반자 입력이 가능합니다. <br />
							&nbsp;&nbsp; •기간을 입력하면 해당기간에 대한 위반사항이 표시됩니다. <br />
						</p>



					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->


			</div>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>




<%@ include file="/WEB-INF/views/outline/footer.jsp"%>
