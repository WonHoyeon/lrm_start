<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.io.PrintWriter"%>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>


<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">

				<div class="callout callout-info" style="width: 30%; align: center">
					<form class="form-horizontal" method="post" action="v1_check">
						<div class="input-group input-group-sm mb-0">
							<input class="form-control form-control-sm"
								placeholder="이름(일부만 입력 가능)" name="hakbun" maxlength="8">
							<div class="input-group-append">
								<button type="submit" class="btn btn-success">조회</button>
							</div>
						</div>
					</form>
				</div>


				<div class="card">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">외부인 위반내역</h3>
						</div>
					</div>
					<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>No.</th>
									<th>위반일자</th>
									<th>거주지</th>
									<th>이름</th>
									<th>연락처</th>
									<th>비고</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>2020-05-05</td>
									<td>원주시</td>
									<td>강길동</td>
									<td>010-5544-1234</td>
									<td>5.11, 4.88, 2.55</td>
									<td class="text-right py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
											<a href="#" class="btn btn-danger"><i
												class="fas fa-trash"></i></a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->

				<div class="card">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">추가입력</h3>
						</div>
					</div>
					<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>위반일자</th>
									<th>거주지</th>
									<th>이름</th>
									<th>연락처</th>
									<th>비고</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"> <i
													class="far fa-calendar-alt"></i>
												</span>
											</div>
											<input type="text" class="form-control float-right"
												id="reservation">
										</div>
									</td>
									<td><div class="form-group">
											<input type="text" class="form-control" id="region">
										</div></td>
									<td><div class="form-group">
											<input type="text" class="form-control" id="name">
										</div></td>
									<td><div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i
													class="fas fa-phone"></i></span>
											</div>
											<input type="text" class="form-control"
												data-inputmask="'mask': ['999-9999-9999']"
												data-mask>
										</div></td>
										<td><div class="form-group">
											<input type="text" class="form-control" id="details">
										</div></td>

									<td class="text-right py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a href="#" class="btn btn-info"><i class="fas fa-eye"></i></a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>


				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">위반자 입력(외부인)</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool"
								data-card-widget="collapse" data-toggle="tooltip"
								title="Collapse">
								<i class="fas fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body p-0">
						<p>
							<br /> &nbsp;&nbsp; •학생이 아닌 외부이용자들을 등록하는 메뉴입니다. <br /> &nbsp;&nbsp;
							•중복확인 후 등록되지 않은 자료만 입력하기 바랍니다. <br /> &nbsp;&nbsp;
							•추가입력 시 자료입력 확인을 위해 중복확인이 추가입력된 자료의 ID로 자동 변경됩니다.<br />
						</p>



					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->


			</div>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>

<%@ include file="/WEB-INF/views/outline/footer.jsp"%>