<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>

<html lang="en">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<head>
	<title>연세대학교 | 도서관 이용수칙 위반 관리 시스템</title>
	<meta charset="UTF-8">
	<meta name="description" content="loans HTML Template">
	<meta name="keywords" content="loans, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Favicon -->
	<link href="resources/img/favicon.ico" rel="shortcut icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
 
	<!-- Stylesheets -->
	<link rel="stylesheet" href="resources/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="resources/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="resources/css/owl.carousel.min.css"/>
	<link rel="stylesheet" href="resources/css/flaticon.css"/>
	<link rel="stylesheet" href="resources/css/slicknav.min.css"/>

	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="resources/css/style.css"/>
	  <!-- Font Awesome -->
  <link rel="stylesheet" href="resources/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="resources/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="resources/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- daterange picker -->
  <link rel="stylesheet" href="resources/plugins/daterangepicker/daterangepicker.css">
    <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="resources/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="resources/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="resources/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="resources/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
</head>


<body>
		<%
			String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String)session.getAttribute("userID");   //로그인 된 사용자 기억하기
		}
		%>


	<!-- Header Section -->
	<header class="header-section">
		<a href="index" class="site-logo">
			<img src="resources/img/logo.png" alt="" style="width:300px">
		</a>
		<nav class="header-nav" style="max-height:100px">
		
			<ul class="main-menu" style="margin-top:5px;">
				<li><a href="violation_id">위반자 입력(개인별)</a></li>
				<li><a href="violation_code">위반자 입력(구분별)</a></li>
				<li><a href="violation_out">위반자 입력(외부인)</a></li>
				<li><a href="violation_def">처벌 결정</a></li>
				<li><a href="print_violation">출력</a></li>
					
			<div class="header-right">
			<a href="/logoutAction" class="hr-btn"><i class="flaticon-029-telephone-1"></i>로그아웃</a>
<!--				<div class="hr-btn hr-btn-2">+45 332 65767 42</div> 									-->
			</div>
			</ul>
		</nav>
	</header>
	<!-- Header Section end -->










