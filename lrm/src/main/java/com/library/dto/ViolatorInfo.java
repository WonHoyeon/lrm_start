package com.library.dto;

public class ViolatorInfo {

	private String HAKBUN;
	private String NAME;
	private String SOSOK;
	private String HAKKWA;
	private String JAEHAK;
	
		
	public String getHAKBUN() {
		return HAKBUN;
	}
	public void setHAKBUN(String hAKBUN) {
		HAKBUN = hAKBUN;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getSOSOK() {
		return SOSOK;
	}
	public void setSOSOK(String sOSOK) {
		SOSOK = sOSOK;
	}
	public String getHAKKWA() {
		return HAKKWA;
	}
	public void setHAKKWA(String hAKKWA) {
		HAKKWA = hAKKWA;
	}
	public String getJAEHAK() {
		return JAEHAK;
	}
	public void setJAEHAK(String jAEHAK) {
		JAEHAK = jAEHAK;
	}
	
}
