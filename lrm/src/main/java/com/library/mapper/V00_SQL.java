package com.library.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.library.dto.ViolatorInfo;



public interface V00_SQL {

	 @Select("SELECT sysdate FROM dual")
	    public String getTime();
	 
	 public String getTime2();
	 
	 public List<ViolatorInfo> getInfo(); 
	 
	 
}
