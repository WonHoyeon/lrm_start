package com.library.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.Model;

import com.library.dto.ViolatorInfo;

public interface V00Service {
	
	public ViolatorInfo selectViolationList(Map<String, Object> paramMap) throws Exception;
}