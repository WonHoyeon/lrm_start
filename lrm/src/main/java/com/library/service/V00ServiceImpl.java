package com.library.service;

import java.security.Timestamp;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.library.dao.V00DAO;
import com.library.dao.violation_id_DAO;
import com.library.dto.ViolatorInfo;
import com.library.mapper.V00_SQL;

import lombok.Setter;


@Component
@Service("V00Service")
public class V00ServiceImpl implements V00Service {

//	@Resource(name = "lrmTest")
//	private LrmTest lrmTest;

	    @Resource(name="V00DAO")
	    private V00DAO v00dao;
	

	@Override
	public ViolatorInfo selectViolationList(Map<String, Object> paramMap) throws Exception {
		return v00dao.selectViolationList();
	}


}
