package com.library.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.library.service.V00ServiceImpl;
import com.library.service.V00Service;

/**
 * Servlet implementation class BoardFrontController
 */

@Controller
public class VController {

	
	@Resource(name = "V00ServiceImpl")
	V00Service v00service;

	private static final Logger Log = LoggerFactory.getLogger(VController.class);
	
	
	@RequestMapping("/index")
	public String index() {

		return "index";
	}

	@RequestMapping("/loginAction")
	public String loginAction(HttpServletRequest request, Model model) {

		model.addAttribute("request", request);
		vcom = new V00ServiceImpl();
		vcom.execute(model);

		return "login";
	}

	
	
	@RequestMapping("/V00Check")
	public String go1(Model model) throws Exception {
		
		ModelAndView modelAndView = new ModelAndView();
		try {
			modelAndView.addObject("resultTest", vcom.getResultData());	
		}catch(Exception e) {
			Log.error(e.getMessage());
		}
		
		System.out.println(vcom.getResultData() + "!!!!");

		return "violation_id";
	}

	@RequestMapping("/V00List")
	
	public String go2(Model model, HttpServletRequest request) {
			
		vcom = new V1_Check();
		vcom.execute(model, request.getParameter("hakbun"));
		
		Map<String, Object> map = model.asMap();
		HttpServletRequest request = (HttpServletRequest) map.get("request");
		String hakbun = request.getParameter("hakbun");
		model.addAttribute("hakbun", hakbun);
		
		return "V00List";

	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping("/v1_insert")
	public String v1_insert(HttpServletRequest request, Model model) {

		model.addAttribute("request", request);
		vcom = new V00ServiceImpl();
		vcom.execute(model);

		return "redirect:v1_check";
	}


	@RequestMapping("/violation_code")
	public String go2() {

		return "violation_code";
	}

	@RequestMapping("/violation_def")
	public String go3() {

		return "violation_def";
	}

	@RequestMapping("/violation_out")
	public String go4() {

		return "violation_out";
	}

	@RequestMapping("/print_violation")
	public String go5() {

		return "print_violation";
	}

	@RequestMapping("/print_result")
	public String go6() {

		return "print_result";
	}
}