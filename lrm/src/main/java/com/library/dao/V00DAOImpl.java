package com.library.dao;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.library.dto.ViolatorInfo;
import com.library.mapper.TimeMapper;

import lombok.Setter;


@Repository("V00DAO")
public class V00DAOImpl implements V00DAO {

	@Override
	public ViolatorInfo selectViolationList() throws Exception {
			
		return selectV00List("V00.sql.selectV00");
	}
}